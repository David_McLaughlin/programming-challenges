defmodule Test do

    def rotate_and_compare(str1, str2) do
        for n <- 1..(String.length(str1)), do: str2 == String.slice(str1, n..-1) <> String.slice(str1, 0..(n-1))
    end

    def same_necklace(str1, str2) do
        cond do
            str1 == str2 ->
                true
            String.length(str1) != String.length(str2) ->
                false
            true ->
                true in rotate_and_compare str1, str2
        end
    end

    def bonus_1(str) do
        Enum.count (rotate_and_compare str, str), &(&1)
    end
end