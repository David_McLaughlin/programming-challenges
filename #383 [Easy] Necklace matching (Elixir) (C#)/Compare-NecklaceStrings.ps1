<#
    .SYNOPSIS
    Compares two 'necklace' strings, returning $true if they match and $false if they do not match.

    .DESCRIPTION
    Takes two 'necklace' strings and compares to see if they represent the same necklace.
    Generally, two strings describe the same necklace if you can remove some number of letters from the beginning of one, attach them to the end in their original ordering, and get the other string.
    Reordering the letters in some other way does not, in general, produce a string that describes the same necklace.

    .EXAMPLE
    Compare-NecklaceStrings @("test", "estt") -> $True
    Compare-NecklaceStrings @("test", "esta") -> $False
    Compare-NecklaceStrings -NecklaceStringA "test" -NecklaceStringB "estt" -> $True
#>
function Compare-NecklaceStrings
{
    param (
        
        [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$True, ParameterSetName = "twoStrings")]
        [ValidateLength(1, [int]::MaxValue)]
        [string]
        $NecklaceStringA,
        
        [Parameter(Mandatory=$true, Position=1, ValueFromPipeline=$True, ParameterSetName = "twoStrings")]
        [ValidateLength(1, [int]::MaxValue)]
        [string]
        $NecklaceStringB,

        [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$True, ParameterSetName = "array")]
        [ValidateCount(2, 2)]
        [string[]]
        $NecklaceStrings
    )

    if ($PSCmdlet.ParameterSetName -ieq "array") {
        $NecklaceStringA = $NecklaceStrings[0]
        $NecklaceStringB = $NecklaceStrings[1]
    }

    if ($NecklaceStringA.Length -ne $NecklaceStringB.Length) {
        return $false
    }

    if ($NecklaceStringA -ieq $NecklaceStringB) {
        return $true
    }

    $private:isMatch = (0..$NecklaceStringA.Length) | ForEach-Object {
        $private:partA = (-join $NecklaceStringA[$_..$NecklaceStringA.Length])
        $private:partB = (-join $NecklaceStringA[0..($_-1)])
        $private:combination = (($private:partA.Length -lt $NecklaceStringA.Length) ? $(($private:partA, $private:partB) -join '') : $private:partA)

        if ($NecklaceStringB -ieq $private:combination) {
            $true
            break
        }
    }

    $private:isMatch -eq $true ? $true : $false
}