// This script will prompt for two necklace strings then dump true or false if they match.
// See the readme for details on what it means for two strings to represent the same necklace.
// This script targets .NET Core 3.1.400, to run it Roslynpad can be used: https://github.com/aelij/RoslynPad

Console.WriteLine("Enter necklace one");
string necklaceOne = Console.ReadLine();

Console.WriteLine("Enter necklace two");
string necklaceTwo = Console.ReadLine();

// Check the length first in C# as legth is precomputed when the string is created.
// In Elixir we'd need to traverse both strings to calculate their length
if (necklaceOne.Length != necklaceTwo.Length)
{
    false.Dump();
    return;
}

if (necklaceOne.Equals(necklaceTwo))
{
    true.Dump();
    return;
}

for (int i = 0; i < necklaceOne.Length; i++)
{
    var tempString =
        necklaceOne.Substring(i, necklaceOne.Length - i)
        + necklaceOne.Substring(0, i);
        
    if (necklaceTwo == tempString)
    {
        true.Dump();
        return;
    }
}

false.Dump();
