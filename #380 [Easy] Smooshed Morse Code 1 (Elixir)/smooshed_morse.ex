defmodule Smorse do

    def get_morse_lookup, do: %{
        "a" => ".-",
        "b" => "-...",
        "c" => "-.-.",
        "d" => "-..",
        "e" => ".",
        "f" => "..-.",
        "g" => "--.",
        "h" => "....",
        "i" => "..",
        "j" => ".---",
        "k" => "-.-",
        "l" => ".-..",
        "m" => "--",
        "n" => "-.",
        "o" => "---",
        "p" => ".--.",
        "q" => "--.-",
        "r" => ".-.",
        "s" => "...",
        "t" => "-",
        "u" => "..-",
        "v" => "...-",
        "w" => ".--",
        "x" => "-..-",
        "y" => "-.--",
        "z" => "--.."
    }

    def get(str) do
        out = for n <- String.graphemes(str), do: get_morse_lookup()[n |> String.downcase()]
        List.to_string(out |> Enum.filter(& &1))
    end

    def bonus_1 do
        chunk_batch_size = 100
        main_process_id = self()

        (File.stream!("../../enable1.txt")
        |> Stream.chunk_every(chunk_batch_size)
        |> Enum.map(fn(chunk_collection) ->

            # the idea was to process each chunk in a separate process concurrently
            # this doesn't really do that, we spin up the new process and then wait
            # for it to send its output... Going to leave this here as it was a fun
            # challenge to figure out. Might try to redo it later.
            _ = spawn (fn ->

                chunk_collection
                |> Enum.map(&(Smorse.get &1))
                |> (fn(smorsed_output) ->
                    _ = send main_process_id, {:ok, smorsed_output}
                end).()

            end)

            receive do
                {:ok, msg} -> msg
            end

        end))
        |> List.flatten
        |> Enum.reduce(%{}, fn element, acc -> Map.update(acc, element, 1, &(&1 + 1)) end)
        |> Enum.find(fn {_key, val} -> val == 13 end)
        |> elem(0)

    end

    def bonus_2 do
        File.stream!("../../enable1.txt")
        |> Enum.map(&(Smorse.get &1))
        |> Enum.find(fn val -> val |> String.contains?("---------------") end)
    end

end