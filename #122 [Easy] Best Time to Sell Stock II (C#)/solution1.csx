var test1 = new[] { 7, 1, 7, 9, 1, 1, 1, 2, 9, 3, 4, 5, 6, 1, 1, 5, 1, 5, 3, 6, 100 };
var test2 = new[] { 5, 6, 1, 1, 5, 1, 5, 3, 6, 12 };
var test3 = new[] { 12, 11, 10, 9 };

ProfitFromBestMoves(test1).Dump(nameof(test1));
ProfitFromBestMoves(test2).Dump(nameof(test2));
ProfitFromBestMoves(test3).Dump(nameof(test3));

public int ProfitFromBestMoves(int[] prices) 
{
    if (prices == null || prices.Length < 2)
        return 0;
    
    int j = 0, profit = 0;
    for (int i = 1; i < prices.Length; i++, j++)
    {
        var isCurrentLargerThanPrevious = (prices[i] > prices[j]);
        profit += (isCurrentLargerThanPrevious ? (prices[i] - prices[j]) : 0);
    }
    
    return profit;
}