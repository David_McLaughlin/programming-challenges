You are given an array `prices` each element in the array is the price of a given stock on the `ith` day. So `prices[0]` is the price on the first day, `prices[1]` is the price on the second day, etc.

Your goal is to find the maximum profit that can be achieved. You may buy and sell as many times as possible, for instance, you may buy on day 1, sell on day 3, buy on day 10, and sell on day 11, or any combination of days so long as you sell the stock before you buy again.

**Note** We are not trying to calculate net gain/loss of money, instead we are trying to determine the profit of every buy/sell transaction, and what is the largest profit that could be made with optimal buy/sell transactions. e.g. given the input `1, 3, 5, 1`, if we buy on day 1 our profit is *not* `-1`, rather it's `0` because we've made no profit yet. Our net loss is -1 but for this exercise we only care about profit. If we buy on day 1 and sell on day 2, then our profit would be `2`.

**Example 1:**

```Input: prices = [7,1,5,3,6,4]
Output: 7
Explanation: Buy on day 2 (price = 1) and sell on day 3 (price = 5), profit = 5-1 = 4.
Then buy on day 4 (price = 3) and sell on day 5 (price = 6), profit = 6-3 = 3.
```

**Example 2:**

```Input: prices = [1,2,3,4,5]
Output: 4
Explanation: Buy on day 1 (price = 1) and sell on day 5 (price = 5), profit = 5-1 = 4.
Note that you cannot buy on day 1, buy on day 2 and sell them later, as you are engaging multiple transactions at the same time. You must sell before buying again.
```

**Example 3:**

```Input: prices = [7,6,4,3,1]
Output: 0
Explanation: In this case, no transaction is done, i.e., max profit = 0.
```


## Solutions  

### solution1.csx  
**Time complexity:** O(n) - 1 cycle over the input array  
**Space complexity:** O(1) - Memory needed does not change with the size of the input  

**Explanation:**
At the core, all we need to solve this problem is to sum all the profitable moves, this works because the sum of the best move will always be the same (or lower) as the sum of all 'good' moves combined. Say we got `1, 5, 50`, if we buy on day 1, sell on day 2 (+4), buy on day 2, then sell on day 3 (+45), we get `49` as our profit. If we instead bought on day 1, then sold on day 3 (+49) we get `49` total profit. This is true because `5 - 1` makes up the difference of `50 - 5`, no matter how many days we have, the largest peak to valley (`50 - 1`) is always the same or lower than all the smaller moves combined `(5 - 1) + (50 - 5)`. Armed with that understanding the solution becomes fairly simple, although the trick is not obvious until you see it.  

To help visualize this imagine a number line:  

![visual_1](/uploads/47af785144a2247914155335d4c49a27/visual_1.png)  

1 is our valley, and 50 is our peak. Between them are 49 integers.  

![visual_2](/uploads/eb39d722a713e662c1c9b9f5bf7a0055/visual_2.png)  

No matter how many inbetween moves we make, we'll always cover the same distance on our number line, in this case we buy at 1, sell 10, buy at 10 and sell at 50.  

![visual_3](/uploads/ceacf6b7dbe2e4c2acec5b7191e22ede/visual_3.png)  

It isn't always true that our largest peak and valley is the most profitable, sometimes a combination of smaller moves will add up to more than the largest peak to valley. e.g. `1, 5, 60, 6, 50`, `60 - 1 = 59` and `50 - 6 = 44`, combined our profit is `103` which is a good bit more than our largest peak and valley. Logically we can divide that above set into smaller sets where the set as a whole is sequential, e.g. `1, 5, 60` and `6, 50`, then we can sum the profits from these sets to get our maximum profit. If we follow this logic we can further divide our sets into `1, 5`, `5, 60`, `6, 50`, and adding the profit from all sets (`4 + 55 + 44`) gives us the same `103` profit that we got from operating on the larger sets. This works for the same reason layed out in the images above, `5 - 1` + `50 - 45` covers the same distance on the number line as `50 - 1`.  

Hopefully my explaination makes sense and helps you understand why the solution1.csx code works. If you're still having touble, or want to reach out, drop a comment on [this related snippet](https://gitlab.com/David_McLaughlin/programming-challenges/-/snippets/2087437)
