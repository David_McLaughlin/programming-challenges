/*
    P   A   H   N
    A P L S I I G
    Y   I   R
*/
// PAHNAPLSIIGYIR

/*
    P     I    N
    A   L S  I G
    Y A   H R
    P     I
*/
// PINALSIGYAHRPI

/*
    P    H
    A   SI
    Y  I R
    P L  I G
    A    N
*/
// PHASIYIRPLIGAN

Convert("PAYPALISHIRING", 3).Dump();

public string Convert(string s, int numRows)
{
    // Handle our input edge cases, each of these conditions means there
    // is no work for us to do. We could null coalesce to string.Empty
    // or something like throw new NullReferenceException()
    if (s == null || numRows < 2 || s.Length < 2 || s.Length <= numRows)
    {
        return s ?? string.Empty;
    }
    
    int sign = 1, currentRow = 0;
    var outputs = new (char[] characters, int rowIndex)[numRows];
    
    for (int i = 0; i < s.Length; i++)
    {
        // If we're on the first index in a row then we need to initialize
        // the character array.
        if (outputs[currentRow].rowIndex == 0)
            outputs[currentRow].characters = new char[s.Length];
        
        outputs[currentRow].characters[outputs[currentRow].rowIndex++] = s[i];
        
        // To follow the zig-zag we need to keep track of when we hit our 'ends', either 0 or (numRows - 1)
        // if we hit either end then we switch from incrementing to decrementing. 
        // e.g. the currentRow variable should change like this: 0, 1, 2, 3, 2, 1, 0, 1, 2, 3, counting up then down.
        sign = (currentRow == (numRows - 1) || (currentRow == 0 && sign < 0)) ? sign * -1 : sign;
        currentRow += sign;
    }
    
    return outputs.Aggregate((builder: new StringBuilder(s.Length), count: 0), (input, array) =>
    {
        var targetSlice = new ReadOnlySpan<char>(array.characters, 0, array.rowIndex);
        input.builder.Append(targetSlice);
        input.count++; // Track which row we are on so we can get the total count of characters in that row (i.e. array.rowIndex)
        return input;
    }).builder.ToString();
}