defmodule SentenceTools do

    def longest_sentence(str) do
        String.splitter(str, [".", "?", "!"], trim: true) |> # trim: true to remove empty entrires, will prevent extra entries if there is an ellipsis
        Enum.map(fn(sentence) -> {String.trim(sentence), String.splitter(sentence, " ", trim: true) |> Enum.count} end) |>
        Enum.max_by(fn(sentence_tuple) -> elem(sentence_tuple, 1) end)
    end

end